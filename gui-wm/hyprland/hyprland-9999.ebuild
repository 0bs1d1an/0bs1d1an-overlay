# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Dynamic tiling Wayland compositor"
HOMEPAGE="https://github.com/vaxerski/Hyprland"

if [[ ${PV} == *9999 ]]; then
	#MY_WLROOTS_COMMIT="b89ed9015c3fbe8d339e9d65cf70fdca6e5645bc" # master
	inherit git-r3
	EGIT_REPO_URI="https://github.com/vaxerski/Hyprland"
else
	MY_PV="${PV/_/}"
	MY_PN="Hyprland"
	MY_P="${MY_PN}-${MY_PV}"
	MY_WLROOTS_COMMIT="75d31509db8c28e8379fe9570118ef8c82284581" # 0.4.0beta
	SRC_URI="
		https://github.com/vaxerski/Hyprland/archive/v${MY_PV}.tar.gz
		https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/${MY_WLROOTS_COMMIT}/wlroots-${MY_WLROOTS_COMMIT}.tar.gz
	"
	KEYWORDS="~amd64"
	S="${WORKDIR}/${MY_P}"
fi

LICENSE="BSD"
SLOT="0"
IUSE=""

RDEPEND="
	x11-libs/libxcb
	x11-base/xcb-proto
	x11-libs/xcb-util
	x11-libs/xcb-util-keysyms
	x11-libs/libXfixes
	x11-libs/libX11
	x11-libs/libXcomposite
	x11-apps/xinput
	x11-libs/libXrender
	x11-libs/pixman
	dev-libs/wayland-protocols
	gui-libs/wlroots
	x11-libs/cairo
	x11-libs/pango
"
DEPEND="${RDEPEND}"
BDEPEND="
	dev-util/ninja
	sys-devel/gcc
	dev-util/cmake
"

#PATCHES=(
#	"${FILESDIR}/${P}-no-wlroots-building.patch"
#)

src_prepare() {
	if [[ ${PV} != *9999 ]]; then
		rm -rf "${S}/wlroots"
		mv "${WORKDIR}/wlroots-${MY_WLROOTS_COMMIT}/" "${S}/wlroots/"
	fi
	eapply "${FILESDIR}/${P}-fix-field-last_event-has-incomplete-type-output-c.patch"
	eapply "${FILESDIR}/${P}-fix-field-last_event-has-incomplete-type-time-h.patch"
	eapply "${FILESDIR}/${P}-fix-field-last_event-has-incomplete-type-wlr-export-dmabuf-v1-c.patch"
	eapply "${FILESDIR}/${P}-fix-field-last_event-has-incomplete-type-wlr-seat-h.patch"
	eapply "${FILESDIR}/${P}-fix-field-last_event-has-incomplete-type-wlr-screencopy-v1-c.patch"
	head -n -2 Makefile > Makefile.new
	mv Makefile.new Makefile
	eapply_user
}

#src_configure() {
#}

src_compile() {
	emake all
}

#src_test() {
#}

#src_install() {
#}
