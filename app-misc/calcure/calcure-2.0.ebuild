# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{7..10} )

inherit distutils-r1

SRC_URI="https://github.com/anufrievroman/${PN}/archive/refs/tags/${PV}.tar.gz"
DESCRIPTION="Modern TUI calendar and task manager with minimal and customizable UI"
HOMEPAGE="https://github.com/anufrievroman/calcure"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="$(python_gen_any_dep '
	dev-python/holidays[${PYTHON_USEDEP}]
	dev-python/jdatetime[${PYTHON_USEDEP}]
')"
DEPEND="
	${RDEPEND}
"
