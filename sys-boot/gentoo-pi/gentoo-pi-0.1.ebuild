# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

SRC_URI="https://github.com/gkroon/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
DESCRIPTION="A script to install Gentoo on a Raspberry Pi 2/3"
HOMEPAGE="https://github.com/gkroon/gentoo-pi"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
	app-arch/tar
	app-crypt/gnupg
	app-emulation/qemu
	app-shells/bash:*
	dev-vcs/git
	net-misc/curl
	net-misc/rsync
	net-misc/wget
	sys-apps/coreutils
	sys-apps/file
	sys-apps/grep
	sys-apps/kmod
	sys-apps/sed
	sys-apps/util-linux
	sys-block/parted
	sys-fs/cryptsetup
	sys-fs/dosfstools
	sys-fs/e2fsprogs
	sys-libs/ncurses:*
	virtual/awk
"
DEPEND="${RDEPEND}"

#S="${WORKDIR}/${PN}"

src_install() {
	insinto "/usr/share/${PN}"
	doins -r files installer.sh
	fperms 0700 "/usr/share/${PN}/installer.sh"
	newsbin "${FILESDIR}/${PN}.sh" ${PN}
}
