# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an1t3 personal meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="~amd64"

SLOT="0"
LICENSE="BSD"
IUSE="binhost desktop laptop"
REQUIRED_USE="binhost? ( desktop laptop )"

PDEPEND="
	app-admin/ansible
	app-crypt/hashcat-utils
	app-crypt/princeprocessor
	app-misc/calcure
	dev-python/virtualenv
	dev-util/html-packer
	dev-util/shellcheck
	dev-util/yamllint
	net-fs/sshfs
	net-misc/httpie
	net-misc/ipcalc
	net-misc/urlscan
	sys-boot/gentoo-pi
	desktop? (
		0bs1d1an/0bs1d1an-emulation[docker,qemu]
	)
	laptop? (
		0bs1d1an/0bs1d1an-emulation[docker,virtualbox-guest,vmware-guest]
	)
"
