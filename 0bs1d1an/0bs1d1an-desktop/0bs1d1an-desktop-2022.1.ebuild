# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an desktop and all things X meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="amd64"

SLOT="0"
LICENSE="BSD"
IUSE="X binhost bspwm +fonts hyprland i3wm river sway vm wayland"
REQUIRED_USE="binhost? ( X bspwm i3wm vm )"

PDEPEND="
	gui-libs/display-manager-init
	mail-client/neomutt
	media-gfx/feh
	media-gfx/gimp
	media-gfx/scrot
	media-sound/alsa-utils
	media-sound/spotify-tui
	media-sound/spotifyd
	media-tv/kodi
	media-video/mpv
	net-misc/nextcloud-client
	net-misc/remmina
	net-misc/ytfzf
	www-client/firefox
	www-plugins/passff-host
	x11-terms/alacritty
	x11-themes/larry-backgrounds
	x11-themes/papirus-icon-theme
	X? (
		x11-apps/setxkbmap
		x11-apps/xrandr
		x11-base/xorg-server
		x11-misc/dmenu
		x11-misc/picom
		x11-misc/polybar
		x11-misc/redshift
		x11-misc/rofi
		x11-misc/sddm
		x11-misc/urxvt-font-size
		x11-misc/urxvt-perls
		x11-misc/xcompmgr
		x11-misc/xsecurelock
		x11-terms/rxvt-unicode
		bspwm? (
			x11-wm/bspwm
		)
		i3wm? (
			x11-misc/i3blocks
			x11-wm/i3-gaps
		)
	)
	fonts? (
		0bs1d1an/0bs1d1an-fonts
	)
	vm? (
		app-emulation/spice-vdagent
		app-emulation/qemu-guest-agent
	)
	wayland? (
		gui-apps/foot
		gui-apps/tuigreet
		gui-apps/wofi
		hyprland? (
			gui-wm/hyprland
		)
		river? (
			gui-apps/waybar
			gui-wm/river
		)
		sway? (
			gui-wm/sway
		)
	)
"
