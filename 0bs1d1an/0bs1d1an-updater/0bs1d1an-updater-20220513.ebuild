# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Update script for the 0bs1d1an overlay"
HOMEPAGE="https://maelstrom.ninja"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT=""

DEPEND=""

S=${WORKDIR}

src_install() {
	dosbin "${FILESDIR}/0bs1d1an-updater"
}
