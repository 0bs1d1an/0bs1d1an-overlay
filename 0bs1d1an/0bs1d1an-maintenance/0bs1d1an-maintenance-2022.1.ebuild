# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an maintenance meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="amd64"

SLOT="0"
LICENSE="BSD"
IUSE="binhost desktop"
REQUIRED_USE="binhost? ( desktop )"

PDEPEND="
	app-admin/eclean-kernel
	app-admin/sysklogd
	app-eselect/eselect-repository
	app-portage/eix
	app-portage/genlop
	app-portage/gentoolkit
	app-portage/repoman
	dev-util/pkgcheck
	dev-util/pkgdev
	net-fs/nfs-utils
	sys-fs/btrfs-progs
	sys-fs/ncdu
	sys-kernel/gentoo-kernel
	sys-kernel/linux-firmware
	sys-process/cronie
	desktop? (
		sys-apps/bleachbit
	)
"
