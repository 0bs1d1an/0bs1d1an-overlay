# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="8"

KEYWORDS="~amd64"
DESCRIPTION="0bs1d1an meta ebuild to install all apps"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"

SLOT="0"
LICENSE="BSD"
IUSE="binhost desktop dnsserver emulation fonts hare mailserver maintenance misc scanner tor webserver"
REQUIRED_USE="binhost? ( desktop dnsserver emulation fonts hare mailserver maintenance misc scanner tor webserver )"

DEPEND=""
RDEPEND=""
PDEPEND="
	0bs1d1an/0bs1d1an-keyring
	0bs1d1an/0bs1d1an-updater
	desktop? ( 0bs1d1an/0bs1d1an-desktop )
	dnsserver? ( 0bs1d1an/0bs1d1an-dnsserver )
	emulation? ( 0bs1d1an/0bs1d1an-emulation )
	fonts? ( 0bs1d1an/0bs1d1an-fonts )
	hare? ( 0bs1d1an/0bs1d1an-hare )
	mailserver? ( 0bs1d1an/0bs1d1an-mailserver )
	maintenance? ( 0bs1d1an/0bs1d1an-maintenance )
	misc? ( 0bs1d1an/0bs1d1an-misc )
	scanner? ( 0bs1d1an/0bs1d1an-scanner )
	tor? ( 0bs1d1an/0bs1d1an-tor )
	webserver? ( 0bs1d1an/0bs1d1an-webserver )
"
