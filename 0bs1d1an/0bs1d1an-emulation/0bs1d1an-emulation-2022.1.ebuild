# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an emulation meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="amd64"

SLOT="0"
LICENSE="BSD"
IUSE="binhost docker qemu virtualbox-guest vmware-guest"
REQUIRED_USE="binhost? ( docker qemu virtualbox-guest vmware-guest )"

PDEPEND="
	docker? (
		app-containers/docker
		app-containers/docker-cli
		app-containers/docker-compose
	)
	qemu? (
		app-emulation/qemu
		app-emulation/virt-manager
	)
	virtualbox-guest? (
		app-emulation/virtualbox-guest-additions
		app-emulation/virtualbox-modules
	)
	vmware-guest? (
		app-emulation/open-vm-tools
	)
"
