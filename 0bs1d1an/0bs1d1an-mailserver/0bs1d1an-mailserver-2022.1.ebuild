# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an DNS server ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="~amd64"

SLOT="0"
LICENSE="BSD"
IUSE=""

PDEPEND="
	dev-db/redis
	mail-filter/rspamd
	mail-mta/postfix
	net-mail/dovecot
"
