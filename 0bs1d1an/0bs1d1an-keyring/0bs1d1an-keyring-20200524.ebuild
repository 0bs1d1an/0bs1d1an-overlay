# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="OpenPGP keys used for the 0bs1d1an overlay"
HOMEPAGE="https://maelstrom.ninja"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT=""

DEPEND=""

S=${WORKDIR}

src_install() {
	insinto /usr/share/0bs1d1an
	#to make a file with more keys
	#gpg --keyserver keyserver.ubuntu.com --recv 4AEE18F83AFDEB23
	#                     new-dev          gkroon/Obs1d1an
	#gpg --armor --export 0123456789ABCDEF 394C398C531EFAB0 > 0bs1d1an-keyring.asc
	newins "${FILESDIR}/0bs1d1an-keyring.txt" 0bs1d1an-keyring.asc
}
