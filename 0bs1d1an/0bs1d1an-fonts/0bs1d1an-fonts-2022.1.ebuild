# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an fonts meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="amd64"

SLOT="0"
LICENSE="BSD"
IUSE=""

PDEPEND="
	media-fonts/arphicfonts
	media-fonts/baekmuk-fonts
	media-fonts/cozette
	media-fonts/fontawesome
	media-fonts/gohufont
	media-fonts/kochi-substitute
	media-fonts/nerd-fonts
	media-fonts/noto-emoji
	media-fonts/quivira
	media-fonts/roboto
	media-fonts/terminus-font
"
