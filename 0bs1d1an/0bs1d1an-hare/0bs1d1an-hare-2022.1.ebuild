# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an HARE meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="~amd64"

SLOT="0"
LICENSE="BSD"
IUSE="0bs1d1an-full binhost"
REQUIRED_USE="binhost? ( 0bs1d1an-full )"

PDEPEND="
	app-text/aha
	dev-ruby/guard
	dev-ruby/guard-shell
	dev-util/rpg
	dev-util/sr2t
	dev-util/toad
	0bs1d1an-full? (
		app-text/pandoc
		app-text/texlive
		dev-tex/latexmk
		dev-texlive/texlive-latexextra
	)
"
