# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="0bs1d1an miscellaneous tools meta ebuild"
HOMEPAGE="https://gitlab.com/0bs1d1an/0bs1d1an-overlay"
KEYWORDS="~amd64"

SLOT="0"
LICENSE="BSD"
IUSE=""

PDEPEND="
	app-admin/pass
	app-admin/sudo
	app-editors/neovim
	app-forensics/rkhunter
	app-misc/fetsh
	app-misc/neofetch
	app-misc/onefetch
	app-misc/ufetch
	app-shells/fish
	app-shells/fzf
	dev-vcs/git
	net-analyzer/fail2ban
	net-analyzer/nmap
	net-dns/ldns-utils
	net-misc/dhcpcd
	net-misc/netifrc
	net-misc/ntp
	sys-apps/bat
	sys-apps/exa
	sys-apps/mlocate
	sys-apps/ripgrep
	sys-boot/grub:2
	sys-process/bottom
	sys-process/htop
	www-client/browsh
"
