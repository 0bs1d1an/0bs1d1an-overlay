# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
USE_RUBY="ruby23 ruby24 ruby25 ruby26 ruby27"

RUBY_FAKEGEM_TASK_DOC=""
RUBY_FAKEGEM_EXTRADOC=""

RUBY_FAKEGEM_TASK_TEST=""

RUBY_FAKEGEM_GEMSPEC=""

inherit ruby-fakegem

DESCRIPTION="Wrapper libray for notification libraries such as Growl, Libnotify, Notifu"
HOMEPAGE="https://github.com/guard/notiffany"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

ruby_add_bdepend "
	dev-ruby/bundler
"

ruby_add_rdepend "
	>=dev-ruby/nenv-0.3.0
	>=dev-ruby/shellany-0.0
	>=dev-ruby/nenv-0.1
"
