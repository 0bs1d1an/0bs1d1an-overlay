# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
USE_RUBY="ruby23 ruby24 ruby25 ruby26 ruby27"

RUBY_FAKEGEM_TASK_DOC=""
RUBY_FAKEGEM_EXTRADOC="
	README.md
	CHANGELOG.md
"

RUBY_FAKEGEM_TASK_TEST=""

#RUBY_FAKEGEM_GEMSPEC="${PN}.gemspec"

inherit ruby-fakegem

DESCRIPTION="Guard is a CLI tool to easily handle events on file system modifications"
HOMEPAGE="https://github.com/guard/guard"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

ruby_add_rdepend "
	>=dev-ruby/formatador-0.2.4
	>=dev-ruby/listen-2.7.0
	>=dev-ruby/lumberjack-1.0.12
	>=dev-ruby/nenv-0.1
	>=dev-ruby/notiffany-0.0
	>=dev-ruby/pry-0.9.12
	>=dev-ruby/shellany-0.0
	>=dev-ruby/thor-0.18.1
"
