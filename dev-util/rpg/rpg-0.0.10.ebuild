# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{6..10} )

inherit distutils-r1

SRC_URI="https://gitlab.com/0bs1d1an/${PN}/-/archive/${PV}/${P}.tar.gz"
DESCRIPTION="Risk plot generator"
HOMEPAGE="https://gitlab.com/0bs1d1an/rpg"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="$(python_gen_any_dep '
	dev-python/matplotlib[${PYTHON_USEDEP}]
')"
DEPEND="
	${RDEPEND}
"
