# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

SRC_URI="https://gitlab.com/0bs1d1an/${PN}/-/archive/${PV}/${P}.tar.gz"
DESCRIPTION="Packing files inside HTML files"
HOMEPAGE="https://gitlab.com/0bs1d1an/html-packer"
LICENSE="BSD"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"

src_install(){
	newbin html-packer.py html-packer
}
