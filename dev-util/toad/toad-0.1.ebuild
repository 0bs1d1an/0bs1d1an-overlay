# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{5..10} )

SRC_URI="https://gitlab.com/0bs1d1an/${PN}/-/archive/${PV}/${P}.tar.gz"
DESCRIPTION="A tool to convert testssl.sh JSON files to observations in AsciiDoc"
HOMEPAGE="https://gitlab.com/0bs1d1an/toad"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
	dev-ruby/asciidoctor
	net-analyzer/testssl
"
DEPEND="
	${RDEPEND}
"

src_install() {
	newbin ${PN}.py ${PN}
}
